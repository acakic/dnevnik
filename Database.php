<?php


class Database
{
  // DB Params
  /***
   * Protected atribute mozete menjati kroz aplikaciju, bilo kroz child klase ili dodatne metode, sto ne bi smelo da se desava parametrima za konekciju
   * Bolja opcija je definisanje konstantni, bile one na nivou cele aplikacije kroz define() funkciju ili koriscenjem 'private const' u okviru klase
   * 
   */
  protected $host = 'localhost';
  protected $db_name = 'e_diary';
  protected $username = 'root';
  protected $password = '';
  protected $conn;

  // DB Connect
  public function connect()
  {
    $this->conn = null;
    try {
      $this->conn = new PDO('mysql:host=' . $this->host .';dbname=' . $this->db_name, $this->username, $this->password);
      /***
       * procitajte o PDO::ATTR_EMULATE_PREPARES koji je bitan za security ovde:
       * https://stackoverflow.com/questions/134099/are-pdo-prepared-statements-sufficient-to-prevent-sql-injection
       */
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
      echo 'Connection Error: ' . $e->getMessage();
    }
  // $conn->query("SET COLLATION_CONNECTION='utf8_general_ci'");
    return $this->conn;
  }
}
