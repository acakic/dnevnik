    <div class="container">
        <!-- <h1>TEACHER PAGE</h1> -->
        <h1>Dobrodosli <?php echo $_SESSION['user']['first_name'] .' ' .$_SESSION['user']['last_name'];  ?></h1>
        <h1>Odeljenje <a href="/teacher/studentgroup"><?php echo $_SESSION['classroom']['years_title'] .'/' .$_SESSION['classroom']['subgroup_title'];  ?></a></h1>
        <h1><a href="/teacher/teachernotification">Poruke</a></h1>


        <?php if (isset($_SESSION['sg_schedule']) && $_SESSION['sg_schedule'] !== false): ?>
            <div class="tableContainer">
                <?php $cntr = 0; ?>
                <caption>Schedule for <?php echo "group " . $_SESSION['classroom']['years_title'] .'/' .$_SESSION['classroom']['subgroup_title']; ?></caption>
                <table border="1px solid black" cellspacing="0" cellpadding="10">
                    <thead>
                        <tr style="background-color: #7f868a;">
                            <th></th>
                            <th><?php echo "Monday" ?></th>
                            <th><?php echo "Tuesday" ?></th>
                            <th><?php echo "Wednesday" ?></th>
                            <th><?php echo "Thursday" ?></th>
                            <th><?php echo "Friday" ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (View::$data as $subclass): ?>
                            <tr>
                                <td style="background-color: #7f868a;"><b><?php echo ++$cntr; ?>.class</b></td>
                                <td><?php echo $subclass['Monday'] ?></td>
                                <td><?php echo $subclass['Tuesday'] ?></td>
                                <td><?php echo $subclass['Wednesday'] ?></td>
                                <td><?php echo $subclass['Thursday'] ?></td>
                                <td><?php echo $subclass['Friday'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else:?>
            <div>
                <p>Trenutno raspored ne postoji</p>
            </div>
        <?php endif; ?>
     </div> <!-- container end -->