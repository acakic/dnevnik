    <div class="container">
        <h1>Notifications for teacher</h1>
        <?php if (isset($_SESSION['messages']) && $_SESSION['messages'] !== false): ?>
            <div>
                <?php foreach ($_SESSION['messages'] as $value):?>
                    <?php if (isset($value['type']) && $value['type'] == 'message'): ?>
                        <div>
                            <hr>
                            <h1>Messages</h1>
                            <h3><?php echo $value['first_name'] .' '. $value['last_name']?></h3>
                            <h3><?php  ?></h3>
                            <p><?php echo $value['message']?></p>
                            <p style="color:green"><?php echo $value['created_at']?></p>
                            <div class="messageForm">
                                <form action="/admin/sendMessage" method="post">
                                    <!-- // od koga  -->
                                    <input type="hidden" name="to_user" value="<?php echo $value['to_user'] ?>">
                                    <!-- za koga -->
                                    <input type="hidden" name="from_user" value="<?php echo $value['from_user'] ?>">
                                    <!-- type notification or message  -->
                                    <input type="hidden" name="type" value="<?php echo $value['type'] ?>">
                                    <!-- porukaaa -->
                                    <label for="msg">Answer</label><br>
                                    <textarea type= "text" name="msg" id="msg" cols="65" rows="12" style = 'resize: none;'></textarea><br>
                                    <input type="submit" name="sendMessage" value="Send">
                                </form>
                        </div>
                    <?php elseif(isset($value['type']) && $value['type'] == 'notification'): ?>
                        <div>
                            <hr>
                            <h1>Notification</h1>
                            <h3><?php echo $value['first_name'] .' '. $value['last_name']?></h3>
                            <p><?php echo $value['message']?></p>
                            <p style="color:green"><?php echo $value['created_at']?></p>
                        </div>
                    <?php endif; ?>
                <?php endforeach;?>
            </div>
        <?php else:?>
            <div>
                <p>NEMATE PORUKE I OBAVEŠTENJA</p>
            </div>
        <?php endif; ?>
        <a class="btn" href="/teacher/teacherpage">Nazad</a>
    </div> <!-- container end -->