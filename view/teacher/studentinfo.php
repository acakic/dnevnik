    <div class="container">
        <div class="formContainer">
            <img class="avatar" src="../../pictures/avatar.png">
            <h1>Enter grade for student</h1>
            <form name="gradesinput_form" action="/teacher/insertGrades" method="post">  
                <!-- <label for="subject">Choose subject:</label> -->
                <select name="subject">
                    <option value="" disabled selected hidden>Choose Subject</option>
                    <?php foreach ($_SESSION['allSubjects'] as $value) : ?>
                        <option value="<?php echo $value['id_subjects'] ?>"> <?php echo $value['subject_title'] ?></option>
                    <?php endforeach ; ?>
                </select>
                <!-- <label for="grade">Choose grade:</label> -->
                <select name="grade">
                    <option value="" disabled selected hidden>Choose Grade</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <!-- <label for="semestar">Semester:</label> -->
                <select name="semestar">
                    <option value="" disabled selected hidden>Choose Semester</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>
                <input type="hidden" name="<?php echo 'id_student' ?>" value="<?php echo $_GET['id'];?>">
                <input type="submit" name="submit" value="ENTER GRADE">
            </form>
        </div>  <!-- formContainer end -->
        <div class="tableContainer">
            <?php $cntr = 0; ?>
            <table border="1px solid black" cellspacing="0" cellpadding="10">
                <thead>
                    <tr style="background-color: #7f868a;">
                        <th><?php echo '&nbsp'; ?></th>
                        <th>Predmeti</th>
                        <th>I - semestar</th>
                        <th>Zakljucna ocena</th>
                        <th>II - semestar</th>
                        <th>Zakljucna ocena</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($_SESSION['allSubjects'] as $value): ?>
                    <tr>
                        <td style="background-color: #7f868a;"><?php echo ++$cntr; ?>.</td>
                        <td><?php echo $value['subject_title'] ?></td>
                        <td><?php   if (isset($value['grades']) && isset($value['semester'])){        
                                        for ($i = 0, $c = count($value['grades']); $i < $c; $i++) {
                                            if ($value['semester'][$i] == 1) {
                                                echo $value['grades'][$i].' ';
                                            }
                                        }
                                    } else {
                                        echo '';
                                    }
                        ?></td>
                        <td><?php if (isset($value['grades']) && isset($value['semester'])){    
                                $zbir = 0;  
                                $brojac = 0;   
                                for ($i = 0, $c = count($value['grades']); $i < $c; $i++) {
                                    if ($value['semester'][$i] == 1) {
                                        $brojac++;
                                        $zbir += $value['grades'][$i];     
                                    }
                                }
                                if ($zbir == 0) {
                                    echo '';
                                } else {
                                    echo round($zbir/$brojac);
                                }
                            }
                        ?></td>
                    
                        <td><?php if (isset($value['grades']) && isset($value['semester'])){     
                                    for ($i = 0, $c = count($value['grades']); $i < $c; $i++) {
                                        if ($value['semester'][$i] == 2) {
                                            echo $value['grades'][$i].' ';
                                        }
                                    }
                                } else {
                                    echo '';
                                }
                        ?></td>
                        <td><?php if (isset($value['grades']) && isset($value['semester'])){    
                                    $zbir = 0;  
                                    $brojac = 0;   
                                    for ($i = 0, $c = count($value['grades']); $i < $c; $i++) {
                                        if ($value['semester'][$i] == 2) {
                                            $brojac++;
                                            $zbir += $value['grades'][$i];     
                                        }
                                    }
                                    if ($zbir == 0) {
                                        echo '';
                                    } else {
                                        echo round($zbir/$brojac);
                                    }
                                }
                        ?></td>
                    </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <a class="btn" href="/teacher/studentgroup">Go back</a>
    </div> <!-- container end -->
