<div class="container">
		<h1>Edit subgroup page</h1>
		<a class="btn" href="/admin/subgroup">Go back</a>

		<div class="msg">
			<?php var_dump($_GET); ?>
		</div>
		<?php if (isset(View::$data['one_subgroup'])): ?>
			<div class="details">
				<h2>Current Subgroup data</h2>
				<?php foreach (View::$data['one_subgroup'] as $subgroup): ?>
					<p>Id:  <b><?php 		echo $subgroup['id_subgroup']; ?></p></b>
					<p>Subgroup:  <b><?php 	echo ucfirst($subgroup['subgroup_title']); ?></p></b>
				<?php endforeach; ?>
			</div> <!-- end details -->

			<div class="formContainer">
				<img class="avatar" src="../../pictures/avatar.png">
				<h1>Subgroup update</h1>
				<form action="/admin/updatesubgroup" method="post">
					<input type="hidden" name="id_subgroup" value="<?php echo $subgroup['id_subgroup']; ?>">

					<label for="subgroup_title">Subgroup</label>
					<input type="text" name="subgroup_title" placeholder="Change Subgroup" value="">	
			
					<input type="submit" name="edit_subgroup" value="Send">
				</form>
			</div> <!-- end formContainer -->
		<?php endif ?>
	</div>