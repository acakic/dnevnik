    <div class="container">
        <h1>Dobrodosli <?php echo $_SESSION['user']['first_name'] .' ' .$_SESSION['user']['last_name'];  ?></h1>
        <nav>
            <div class="nav-wrapper">
                <ul>
                    <li><a class="" href="/admin/newuserform">Add new user</a></li>
                    <li><a class="" href="/admin/newstudentform">Add new student</a></li>
                    <li><a class="" href="/admin/schedule">Schedule</a></li>
                    <li><a class="" href="/admin/scheduleform">Create Schedule</a></li>
                    <li><a class="" href="/admin/all_us_st">All users and students</a></li>
                    <li><a class="" href="/admin/subjects">Subjects</a></li>
                    <li><a class="" href="/admin/newsubjectform">Add new subject</a></li>
                    <li><a class="" href="/admin/subgroups">Subgroups</a></li>
                    <li><a class="" href="/admin/newsubgroupform">Add new subgroup</a></li>
                    <li><a class="" href="/admin/studentgroups">Studentgroups</a></li>
                    <li><a class="" href="/admin/newstudentgroupform">Add new studentgroup</a></li>
                </ul>
            </div>
        </nav>
    </div>
