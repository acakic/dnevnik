	<div class="container">
		<?php //if ($this->$all_users['users']): ?>
		<?php //var_dump(View::$data['all_students'][3]); ?>


		<?php $all_users = View::$data['all_users']; ?>
		<?php $all_students = View::$data['all_students']; ?>
		<a class="btn" href="/admin/adminpage">Go back</a>
		<div class="tableContainer">
			<?php $cntr = 0; ?>
			<br>
			<caption>Displaying all users</caption>
			<table border="1px solid black" cellspacing="0" cellpadding="10">
				<thead>
					<tr style="background-color: #7f868a;">
						<th><?php echo '&nbsp'; ?></th>
						<th>id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Role</th>
						<th>EDIT</th>
						<th>DELETE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($all_users as $user): ?>
					<tr>
						<td style="background-color: #7f868a;"><?php echo ++$cntr; ?>.</td>
						<td><?php echo $user['id_user'] ?></td>
						<td><?php echo $user['first_name'] ?></td>
						<td><?php echo $user['last_name'] ?></td>
						<td><?php echo $user['email'] ?></td>
						<td><?php echo $user['role_id'] ?></td>
						<td><a class="btn btnedit" href="/admin/edit?id=<?php echo $user['id_user']; ?>">Edit</a></td>
						<td><a class="btn" href="/admin/edit?delete=<?php echo $user['id_user']; ?>">Delete</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		
		<div class="tableContainer">
			<?php $cntr2 = 0; ?>
			<br>
			<caption>Displaying all student</caption>
			<table border="1px solid black" cellspacing="0" cellpadding="10">
				<thead>
					<tr style="background-color: #7f868a;">
						<th><?php echo '&nbsp'; ?></th>
						<th>id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Date of b-day</th>
						<th>Social ID</th>
						<th>Student group</th>
						<th>EDIT</th>
						<th>DELETE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($all_students as $student): ?>
					<tr>
						<td style="background-color: #7f868a;"><?php echo ++$cntr2; ?>.</td>
						<td><?php echo $student['id_student'] ?></td>
						<td><?php echo $student['first_name'] ?></td>
						<td><?php echo $student['last_name'] ?></td>
						<td><?php echo $student['date_of_birth'] ?></td>
						<td><?php echo $student['social_id'] ?></td>
						<td><?php echo $student['student_group_id'] ?></td>
						<td><a class="btn btnedit" href="/admin/edit?id_student=<?php echo $student['id_student']; ?>">Edit</a></td>
						<td><a class="btn" href="/admin/edit?delete=<?php echo $student['id_student']; ?>">Delete</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<a class="btn" href="/admin/adminpage">Go back</a>
	</div>