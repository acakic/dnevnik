<div class="container">
		<h1>New subgroup form</h1>
		
		<div class="formContainer">
            <img class="avatar" src="../../pictures/avatar.png">
			<h1>New subject</h1>
			<form action="/admin/newsubgroup" method="post">
							
				<label for="subgroup_title">Create new subgroup</label>
				<input type="text" name="subgroup_title" placeholder="Create new subgroup" value="">

                <input type="submit" name="create" value="Create">
			</form>
		</div>	<!-- formContainer end -->
		<a class="btn" href="/admin/adminpage">Go back</a>
</div> <!-- container end -->