<div class="container">
		<h1>New studentgroup form</h1>
		
		<div class="formContainer">
            <img class="avatar" src="../../pictures/avatar.png">
			<h1>New studentgroup</h1>
			<form action="/admin/newstudentgroup" method="post">
							
				<label for="start_year">start year</label>
				<input type="text" name="start_year" placeholder="start year" value="">
                <label for="finish_year">finish year</label>
				<input type="text" name="finish_year" placeholder="finish year" value="">
                <label for="main_teacher_id">main teacher id</label>
				<input type="text" name="main_teacher_id" placeholder="main teacher id" value="">
                <label for="id_years">year</label>
				<input type="text" name="id_years" placeholder="year" value="">
                <label for="id_subgroup">subgroup</label>
				<input type="text" name="id_subgroup" placeholder="subgroup" value="">

                <input type="submit" name="create" value="Create">
			</form>
		</div>	<!-- formContainer end -->
		<a class="btn" href="/admin/adminpage">Go back</a>
</div> <!-- container end -->