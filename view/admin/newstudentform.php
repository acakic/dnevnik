	<div class="container">
		<h1>New student form</h1>
		
		<div class="formContainer">
            <img class="avatar" src="../../pictures/avatar.png">
			<h1>Registrate Student</h1>
			<form action="/admin/studentRegistration" method="post">
							
				<label for="first_name">First Name</label>
				<input type="text" name="first_name" placeholder="Enter First Name" value="">
							
				<label for="last_name">Last Name</label>
				<input type="text" name="last_name" placeholder="Enter Last Name" value="">

                <label for="date_of_birth">Birthday</label>
				<input type="date" name="date_of_birth" placeholder="Enter Birthday" value="">

                <label for="social_id">Social ID</label>
                <input type="number"  name="social_id" placeholder="Must insert 13 characters" value="">

				<label for="student_group_id">Student group</label>
                <select name="student_group_id">
                <option value="" disabled selected hidden>Choose Student group</option>
                <?php foreach (View::$data['all_studentgroups'] as $value) : ?>
                    <option value="<?php echo $value['id_student_group'] ?>"> <?php echo $value['id_years'].'/'.$value['id_subgroup'] ?></option>
                <?php endforeach ; ?>
                </select>  
              <!--  <select name="student_group_id">
                    <option value="" disabled selected hidden>Choose Student group</option>
                    <option value="1">I-1</option>
                    <option value="2">I-2</option>
                    <option value="3">I-3</option>
                    <option value="4">II-1</option>
                    <option value="5">II-2</option>
                    <option value="6">II-3</option>
                    <option value="7">III-1</option>
                    <option value="8">III-2</option>
                    <option value="9">III-3</option>
                    <option value="10">IV-1</option>
                    <option value="11">IV-2</option>
                    <option value="12">IV-3</option>
                    <option value="13">V-1</option>
                    <option value="14">V-2</option>
                    <option value="15">V-3</option>
                    <option value="16">VI-1</option>
                    <option value="17">VI-2</option>
                    <option value="18">VI-3</option>
                    <option value="19">VII-1</option>
                    <option value="20">VII-2</option>
                    <option value="21">VII-3</option>
                    <option value="22">VIII-1</option>
                    <option value="23">VIII-2</option>
                    <option value="24">VIII-3</option>
                </select>  -->

				<input type="submit" name="registrate" value="Registrate">
			</form>
		</div>	<!-- formContainer end -->
		<a class="btn" href="/admin/adminpage">Go back</a>
	</div> <!-- container end -->