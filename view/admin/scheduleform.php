	<div class="container">
		<h1>Create Schedule</h1>
		
		<div>

		<form name="create_schedule" action="/admin/newSchedule" method="post">  

			<select name="studentgroup">
                    <option value="" disabled selected hidden>Choose Student group</option>
                    <?php foreach (View::$data['all_studentgroups'] as $value) : ?>
                        <option value="<?php echo $value['id_student_group'] ?>"> <?php echo $value['id_years'].'/'.$value['id_subgroup'] ?></option>
                    <?php endforeach ; ?>
			</select>

			<select name="semester">  
					<option value="" disabled selected hidden>Choose semester</option>
					<option value="1">1</option>
					<option value="2">2</option>
			</select>
			 
			<br>

        	<table border="2" style= "background-color: #84ed86; color: #761a9b; margin: 0 auto;" >
			<?php $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']; ?>
				<thead>
					<tr>
            		<th><b>CLASS</b></th>  
					<?php foreach ($days as $day) {
                    echo "<th><b>$day</b></th>";
					} ?>
              		</tr>
				</thead>	  
            	<tbody>
				<?php for ($i = 1; $i <= 7 ; $i++) : ?>  
					<tr> 
						<td> <?php echo "class ". $i . "<br>" ?> </td>
						<?php //for ($i = 1; $i <= 5 ; $i++) : ?>
						<?php foreach ($days as $day) : ?>
						   	<td>
							    <input type="hidden" name="class" value="<?php echo $i ?>"> 
							  	<input type="hidden" name="day" value="<?php echo $day ?>">

								<select name="lecturersubjects">  
								<option value="" disabled selected hidden>Choose id_lecturer_subject</option>
								<?php foreach (View::$data['all_lecturer_subjects'] as $value) : ?>  
								<option value="<?php echo $value['id_lecturer_subject'] ?>"> <?php echo $value['id_lecturer_subject'] ?></option>
								<?php endforeach ; ?>
								</select> 

								<select name="type">  
								<option value="NULL" disabled selected hidden>Type NULL or choose</option>  <!-- Je l' moze ovo NULL kao default da stoji ovde?  -->
								<option value="opendor">opendor</option>
								<option value="celebration">celebration</option>
								<option value="dayoff">dayoff</option>
								</select>
							</td>
						<?php endforeach; ?>	
					</tr>
				<?php endfor; ?>	
                </tbody> <br>
            </table>
			<br>
			<input type="submit" name="submit" value="ENTER">
		</form>
		</div>	<!--  end -->
		<br>
		<a class="btn" href="/admin/adminpage">Go back</a>
	</div> <!-- container end -->
