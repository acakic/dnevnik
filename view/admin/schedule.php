<div class="container">
        <h1>Schedule </h1>
        
        <div class="formContainer">
          
            <form name="choose_studentgroup" action="/admin/schedule" method="post">   
                <select name="studentgroup">
                    <option value="" disabled selected hidden>Choose Student group</option>
                    <?php foreach (View::$data['all_studentgroups'] as $value) : ?>
                        <option value="<?php echo $value['id_student_group'] ?>"> <?php echo $value['id_years'].'/'.$value['id_subgroup'] ?></option>
                    <?php endforeach ; ?>
                </select>
                <input type="submit" name="submit" value="ENTER">
            </form>
 </div> <!-- container end -->

 <div class="container">
        
        <?php if (isset($_SESSION['sg_schedule']) && $_SESSION['sg_schedule'] !== false): ?>
            <div class="tableContainer">
                <?php $cntr = 0; ?>              
                <caption>Schedule</caption>
                <table border="1px solid black" cellspacing="0" cellpadding="10">
                    <thead>
                        <tr style="background-color: #7f868a;">
                            <th></th>
                            <th><?php echo "Monday" ?></th>
                            <th><?php echo "Tuesday" ?></th>
                            <th><?php echo "Wednesday" ?></th>
                            <th><?php echo "Thursday" ?></th>
                            <th><?php echo "Friday" ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (View::$data as $subclass): ?>   <!-- ovo puni metoda schedule iz AdminControllera.-->
                            <tr>
                                <td style="background-color: #7f868a;"><b><?php echo ++$cntr; ?>.class</b></td>
                                <td><?php echo $subclass['Monday'] ?></td>
                                <td><?php echo $subclass['Tuesday'] ?></td>
                                <td><?php echo $subclass['Wednesday'] ?></td>
                                <td><?php echo $subclass['Thursday'] ?></td>
                                <td><?php echo $subclass['Friday'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else:?>
            <div>
                <p>Trenutno raspored ne postoji</p>
            </div>
        <?php endif; ?>
     </div> <!-- container end -->