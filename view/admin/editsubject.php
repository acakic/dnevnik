<div class="container">
		<h1>Edit subject page</h1>
		<a class="btn" href="/admin/subjects">Go back</a>

		<div class="msg">
			<?php var_dump($_GET); ?>
		</div>
		<?php if (isset(View::$data['one_subject'])): ?>
			<div class="details">
				<h2>Current Subject data</h2>
				<?php foreach (View::$data['one_subject'] as $subject): ?>
					<p>Id:  <b><?php 		echo $subject['id_subjects']; ?></p></b>
					<p>Subject:  <b><?php 	echo ucfirst($subject['subject_title']); ?></p></b>
				<?php endforeach; ?>
			</div> <!-- end details -->

			<div class="formContainer">
				<img class="avatar" src="../../pictures/avatar.png">
				<h1>Subject update</h1>
				<form action="/admin/updatesubject" method="post">
					<input type="hidden" name="id_subjects" value="<?php echo $subject['id_subjects']; ?>">

					<label for="subject_title">Subject</label>
					<input type="text" name="subject_title" placeholder="Change Subject" value="">	
			
					<input type="submit" name="edit_subject" value="Send">
				</form>
			</div> <!-- end formContainer -->
		<?php endif ?>
	</div>