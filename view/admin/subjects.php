<div class="container">
		<?php //if ($this->$all_users['users']): ?>
		<?php //var_dump(View::$data['all_students'][3]); ?>


		<?php $subjects = View::$data['all_subjects']; ?>

		<a class="btn" href="/admin/adminpage">Go back</a>
		<div class="tableContainer">
			<?php $cntr = 0; ?>
			<br>
			<caption>Subjects</caption>
			<table border="1px solid black" cellspacing="0" cellpadding="10">
				<thead>
					<tr style="background-color: #7f868a;">
						<th><?php echo '&nbsp'; ?></th>
						<th>id</th>
						<th>Subject</th>
						<th>EDIT</th>
						<th>DELETE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($subjects as $subject): ?>
					<tr>
						<td style="background-color: #7f868a;"><?php echo ++$cntr; ?>.</td>
						<td><?php echo $subject['id_subjects'] ?></td>
						<td><?php echo $subject['subject_title'] ?></td>
						<td><a class="btn btnedit" href="/admin/editsubject?id=<?php echo $subject['id_subjects']; ?>">Edit</a></td>
						<td><a class="btn" href="/admin/editsubject?delete=<?php echo $subject['id_subjects']; ?>">Delete</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		
		<a class="btn" href="/admin/adminpage">Go back</a>
	</div>