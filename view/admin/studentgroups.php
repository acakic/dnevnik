<div class="container">
	
		<?php $studentgroups = View::$data['all_studentgroups']; ?>

		<a class="btn" href="/admin/adminpage">Go back</a>
		<div class="tableContainer">
			<?php $cntr = 0; ?>
			<br>
			<caption>Student groups</caption>
			<table border="1px solid black" cellspacing="0" cellpadding="10">
				<thead>
					<tr style="background-color: #7f868a;">
						<th><?php echo '&nbsp'; ?></th>
						<th>id</th>
						<th>start year</th>
                        <th>finish year</th>
                        <th>main teacher id</th>
                        <th>year</th>
                        <th>subgroup</th>
						<th>EDIT</th>
						<th>DELETE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($studentgroups as $studentgroup): ?>
					<tr>
						<td style="background-color: #7f868a;"><?php echo ++$cntr; ?>.</td>
						<td><?php echo $studentgroup['id_student_group'] ?></td>
						<td><?php echo $studentgroup['start_year'] ?></td>
                        <td><?php echo $studentgroup['finish_year'] ?></td>
                        <td><?php echo $studentgroup['main_teacher_id'] ?></td>
                        <td><?php echo $studentgroup['id_years'] ?></td>
                        <td><?php echo $studentgroup['id_subgroup'] ?></td>
						<td><a class="btn btnedit" href="/admin/editstudentgroup?id=<?php echo $studentgroup['id_student_group']; ?>">Edit</a></td>
						<td><a class="btn" href="/admin/editstudentgroup?delete=<?php echo $studentgroup['id_student_group']; ?>">Delete</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		
		<a class="btn" href="/admin/adminpage">Go back</a>
	</div>