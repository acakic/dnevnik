<div class="container">
		<?php //if ($this->$all_users['users']): ?>
		<?php //var_dump(View::$data['all_students'][3]); ?>


		<?php $subgroups = View::$data['all_subgroups']; ?>

		<a class="btn" href="/admin/adminpage">Go back</a>
		<div class="tableContainer">
			<?php $cntr = 0; ?>
			<br>
			<caption>Subgroups</caption>
			<table border="1px solid black" cellspacing="0" cellpadding="10">
				<thead>
					<tr style="background-color: #7f868a;">
						<th><?php echo '&nbsp'; ?></th>
						<th>id</th>
						<th>Subgroup</th>
						<th>EDIT</th>
						<th>DELETE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($subgroups as $subgroup): ?>
					<tr>
						<td style="background-color: #7f868a;"><?php echo ++$cntr; ?>.</td>
						<td><?php echo $subgroup['id_subgroup'] ?></td>
						<td><?php echo $subgroup['subgroup_title'] ?></td>
						<td><a class="btn btnedit" href="/admin/editsubgroup?id=<?php echo $subgroup['id_subgroup']; ?>">Edit</a></td>
						<td><a class="btn" href="/admin/editsubgroup?delete=<?php echo $subgroup['id_subgroup']; ?>">Delete</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		
		<a class="btn" href="/admin/adminpage">Go back</a>
	</div>