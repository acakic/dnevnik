<div class="container">
		<h1>Edit studentgroup page</h1>
		<a class="btn" href="/admin/studentgroup">Go back</a>

		<div class="msg">
			<?php var_dump($_GET); ?>
		</div>
		<?php if (isset(View::$data['one_studentgroup'])): ?>
			<div class="details">
				<h2>Current Studentgroup data</h2>
				<?php foreach (View::$data['one_studentgroup'] as $studentgroup): ?>
					<p>Id:  <b><?php 		echo $studentgroup['id_student_group']; ?></p></b>
					<p>start year:  <b><?php 	echo ucfirst($studentgroup['start_year']); ?></p></b>
                    <p>finish year:  <b><?php 	echo ucfirst($studentgroup['finish_year']); ?></p></b>
                    <p>main_teacher_id:  <b><?php 	echo ucfirst($studentgroup['main_teacher_id']); ?></p></b>
                    <p>year:  <b><?php 	echo ucfirst($studentgroup['id_years']); ?></p></b>
                    <p>subgroup:  <b><?php 	echo ucfirst($studentgroup['id_subgroup']); ?></p></b>
				<?php endforeach; ?>
			</div> <!-- end details -->

			<div class="formContainer">
				<img class="avatar" src="../../pictures/avatar.png">
				<h1>Studentgroup update</h1>
				<form action="/admin/updatestudentgroup" method="post">
					<input type="hidden" name="id_studentgroup" value="<?php echo $studentgroup['id_student_group']; ?>">

					<label for="start_year">start year</label>
					<input type="text" name="start_year" placeholder="Change start year" value="">	
                    <label for="finish_year">finish year</label>
					<input type="text" name="finish_year" placeholder="Change finish year" value="">
                    <label for="main_teacher_id">main teacher id</label>
					<input type="text" name="main_teacher_id" placeholder="Change main teacher id" value="">
                    <label for="id_years">id years</label>
					<input type="text" name="id_years" placeholder="Change id years" value="">
                    <label for="id_subgroup">id subgroup</label>
					<input type="text" name="id_subgroup" placeholder="Change id subgroup" value="">
			
					<input type="submit" name="edit_subgroup" value="Send">
				</form>
			</div> <!-- end formContainer -->
		<?php endif ?>
	</div>