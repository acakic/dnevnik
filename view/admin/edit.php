	<div class="container">
		<h1>Edit  page</h1>
		<a class="btn" href="/admin/all_us_st">Go back</a>

		<div class="msg">
			<?php var_dump($_GET); ?>
		</div>
		<?php if (isset(View::$data['one_user'])): ?>
			<div class="details">
				<h2>Current User data</h2>
				<?php //var_dump(View::$data['one_user']); 

				foreach (View::$data['one_user'] as $user): ?>
					<p>Id:  <b><?php 		echo $user['id_user']; ?></p></b>
					<p>First Name:  <b><?php 	echo ucfirst($user['first_name']); ?></p></b>
					<p>Last Name:  <b><?php 	echo ucfirst($user['last_name']); ?></p></b>
					<p>Email:  <b><?php 		echo $user['email']; ?></p></b>
					<p>Role:  <b><?php 			echo ucfirst($user['role_id']); ?></p></b>
				<?php endforeach; ?>
			</div> <!-- end details -->
			<div class="formContainer">
				<img class="avatar" src="../../pictures/avatar.png">
				<h1>Updating User</h1>
				<form action="/admin/update" method="post">
					<input type="hidden" name="id_user" value="<?php echo $user['id_user']; ?>">

					<label for="first_name">First Name</label>
					<input type="text" name="first_name" placeholder="Change First Name" value="">	
											
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" placeholder="Change Last Name" value="">

					<label for="email">E-mail</label>
					<input type="email" name="email" placeholder="Change email" value="">
					
					<label for="password">Password</label>
					<input type="password" name="password" placeholder="Change Password">

					<label for="role_id">Role</label>
			            <select name="role_id">
			            	<option value="" disabled selected hidden>Choose Role</option>
				            <option value="1">Administrator</option>
				            <option value="2">Director</option>
				            <option value="3">Professor</option>
				            <option value="4">Teacher</option>
				            <option value="5">Parent</option>
				        </select>

					<input type="submit" name="edit_user" value="Send">
				</form>
			</div> <!-- end formContainer -->
		<?php else: ?>
			<div class="details">
				<h2>Current Student data</h2>
				<?php foreach (View::$data['one_student'] as $student): ?>
					<p>Id:  <b><?php 		echo $student['id_student']; ?></p></b>
					<p>First Name:  <b><?php 	echo ucfirst($student['first_name']); ?></p></b>
					<p>Last Name:  <b><?php 	echo ucfirst($student['last_name']); ?></p></b>
					<p>Date of birth:  <b><?php 	echo ucfirst($student['date_of_birth']); ?></p></b>
					<p>Social Id:  <b><?php 	echo ucfirst($student['social_id']); ?></p></b>
					<p>Student group:  <b><?php 	echo ucfirst($student['student_group_id']); ?></p></b>
				<?php endforeach; ?>
			</div> <!-- end details -->
			<div class="formContainer">
				<img class="avatar" src="../../pictures/avatar.png">
				<h1>Updating Student</h1>
				<form action="/admin/updateStudent" method="post">
					<input type="hidden" name="id_student" value="<?php echo $student['id_student']; ?>">

					<label for="first_name">First Name</label>
					<input type="text" name="first_name" placeholder="Change First Name" value="">	
											
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" placeholder="Change Last Name" value="">

					<label for="date_of_birth">Birthday</label>
					<input type="date" name="date_of_birth" placeholder="Change birthday" value="">
					
					<label for="social_id">Social ID</label>
		                <input type="number"  name="social_id" placeholder="Must insert 13 characters" value="">

					<label for="student_group_id">Student group</label>
					<select name="student_group_id">
                	<option value="" disabled selected hidden>Choose Student group</option>
                	<?php foreach (View::$data['all_studentgroups'] as $value) : ?>
                    <option value="<?php echo $value['id_student_group'] ?>"> <?php echo $value['id_years'].'/'.$value['id_subgroup'] ?></option>
               		<?php endforeach ; ?>
               		</select> 
					   
		             <!--   <select name="student_group_id">
		                    <option value="" disabled selected hidden>Choose Student group</option>
		                    <option value="1">I-1</option>
		                    <option value="2">I-2</option>
		                    <option value="3">I-3</option>
		                    <option value="4">II-1</option>
		                    <option value="5">II-2</option>
		                    <option value="6">II-3</option>
		                    <option value="7">III-1</option>
		                    <option value="8">III-2</option>
		                    <option value="9">III-3</option>
		                    <option value="10">IV-1</option>
		                    <option value="11">IV-2</option>
		                    <option value="12">IV-3</option>
		                    <option value="13">V-1</option>
		                    <option value="14">V-2</option>
		                    <option value="15">V-3</option>
		                    <option value="16">VI-1</option>
		                    <option value="17">VI-2</option>
		                    <option value="18">VI-3</option>
		                    <option value="19">VII-1</option>
		                    <option value="20">VII-2</option>
		                    <option value="21">VII-3</option>
		                    <option value="22">VIII-1</option>
		                    <option value="23">VIII-2</option>
		                    <option value="24">VIII-3</option>
		                </select>     -->
					<input type="submit" name="edit_student" value="Send">
				</form>
			</div> <!-- end formContainer -->
		<?php endif ?>
	</div>