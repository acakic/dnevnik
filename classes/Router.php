<?php 

class Router
{
	private $request;
	public $controller;
	public $method;
	public $params;
	//stoje imena controllera malim slovima bez dodatka controller
	/***
	 * Ovo ne bi trebalo da stoji ovde, zamislite aplikaciju sa par stotina strana
	 * Najbolje bi bilo da se ovo kroz bazu resava ali za pocetak bi mogli da napravite fajl koji vraca ovaj niz, isto vazi i za allowed_controllers
	 */
    private $allowed_routes = array(
        'login',
        'noscript',
//		admin pages
        'adminpage',
        'newuserform',
        'newstudentform',
        'scheduleform',
        'all_us_st',
        'edit',
        'update',
        'updateStudent',
        'registration',
		'studentRegistration',
		'subjects',
		'editsubject',
		'updatesubject',
		'newsubject',
		'newsubjectform',
		'subgroups',
		'editsubgroup',
		'updatesubgroup',
		'newsubgroup',
		'newsubgroupform',
		'studentgroups',
		'editstudentgroup',
		'updatestudentgroup',
		'newstudentgroup',
		'newstudentgroupform',
		'schedule',
		'newSchedule',
//		principal pages
        'directorpage',
//		parent pages
        'parentpage',
        'getStudent',
//		teacher pages
        'teacherpage',
        'studentgroup',
		'studentinfo',
		'teachernotification',
		'insertGrades',
		'sendMessage',
//		professor pages
        'professorpage',
        'loginuser',
        'logout',
    );
    private $allowed_controllers = array(
		'teacher',
		'admin',
		'principal',
		'parent',
		'professor',
		'login'
	);
    // kada se napravi metoda, dodati stranicu u allowed routes
    public function __construct($request)
	{
		$this->request = $request;
		$this->getController();
		$this->getMethod();
		$this->getParams();
		//instancira objekat i dodaje mu parametre
		call_user_func_array([$this->controller, $this->method], $this->params);
	}
	private function getController()
	{
		if (isset($_SESSION['rola'])) {
		    if (!in_array($_SESSION['rola']['role_description'], $this->allowed_controllers)) {
				$controller_slug = 'login';
				return;
			}
			$controller_slug = $_SESSION['rola']['role_description'];
        }else{
            $controller_slug = 'login';
        }
		//ovde proveravamo da li se nalazi u tom nizu
		return $this->controller = $this->instantiateController($controller_slug);
	}
	private function instantiateController($controller_slug)
	{
		//setting the name for the controller, ovde dodajemo taj dodatak
		$controller_name = ucfirst($controller_slug) . 'Controller';
		require_once('./controller/' . $controller_name .'.php');
		return new $controller_name($this->request);
	}
	private function getMethod()
	{
		// if is set method in url check if that method exists or show 404 page
		// if is not set show login page
		/***
		 * ovo bi moglo jednim if-om da se resi
		 * $method bi npr. mogao da se definise sa null coalescing operatorom
		 * Umesto da se koristi 
		 * 		if(isset($variable)) {
		 * 	  		$attr = $variable
		 * 		}
		 * Elegantnije resenje bi bilo 
		 * $attr = $variable ?? 'blabla';
		 * $attr bi se popunio vrednoscu iz $variable ukoliko je ta promeljiva setovana a ukoliko nije popunio bi se sa 'blabla'
		 * Pokusajte da koristitite null coalescing i ternarni operator kad god mozete jer je kod citiljivi, ali izbegavajte da ih 'chain'-ujete
		 */
		if (isset($this->request->url_parts[1])) {
			$method = $this->request->url_parts[1];
			if (!method_exists($this->controller, $method)) {
				$this->show404();
			}else if(!in_array($method, $this->allowed_routes)){
				$this->show404();
			}else{
				// unset($this->request->url_parts[1]);
				$this->method = $method;
			}
		}else{
			$this->method = 'login';
		}
	}
	private function getParams()
	{
        $this->params = (isset($this->request->url_parts[3])) ? $this->request->url_parts[3] : array();
	}
	private function show404()
	{
		View::load('login', '404');
	}
}

