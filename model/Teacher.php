<?php

class Teacher
{
    private $conn = null;
    //public $day;
    //public $class;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    /*
     * Method for returning data for classroom for teacher.
     */
    public function getClassroom($id_user)
    {
        $query = 'select y.years_title, sbg.subgroup_title, sg.id_student_group';    // dodato sg.id_student_group na Irena granu !!!
        $query .= ' from years as y ';
        $query .= ' join student_group as sg on y.id_years = sg.id_years';
        $query .= ' join subgroups as sbg on sbg.id_subgroup = sg.id_subgroup';
        $query .= ' join users as u on sg.main_teacher_id = u.id_user';
        $query .= ' where u.id_user  = :id_user';

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_user', $id_user);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for returning all studnets for same classroom.
     */
    public function getAllstudents($id_user)
    {
        $query = 'select *';
        $query .= ' from students as s ';
        $query .= ' join student_group as sg on sg.id_student_group = s.student_group_id';
        $query .= ' where sg.main_teacher_id = :id_user';

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_user', $id_user);
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        var_dump($result);
        if ($result) {
            return $result;
        }
        return false;

    }


    /* 
     *  Method for returning all the grades for one student and subjectname
    */
    public function getSubjects($id)
    {
        $query = 'select sbj.subject_title, sg.id_student_group, sg.id_years, sg.id_subgroup, sbj.id_subjects';
        $query .= ' from subjects as sbj';
        $query .= ' join lecturer_subjects as ls on ls.id_subject = sbj.id_subjects';
        $query .= ' join users as u on ls.id_user = u.id_user';
        $query .= ' join student_group as sg on u.id_user = sg.main_teacher_id';
        $query .= ' join students as s on sg.id_student_group = s.student_group_id';
        $query .= ' where s.id_student  = :id_student';
    
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_student', $id);
        $stmt->execute();
        $subjects = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //  returning all grades for one classroom  and pushing in an empty array. ($allSubjects)
        $allSubjects = [];
    //  nummeric key changing with id_subject for subject (predmet)
        foreach ($subjects as $subject) {
            $allSubjects[$subject['id_subjects']] = $subject;
        }
    //  returning all grades for one student.
        $query = 'select *  from grades where id_student = ' . $id;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $grades = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //  inserting in array with subjects grades as key grades. And connecting with id subject, becouse we have id subject in grade.
        foreach ($grades as $grade){
           // $allSubjects[$grade['id_subjects']]['semester'][$grade['semestar']][] = $grade['grade'];
            $allSubjects[$grade['id_subjects']]['grades'][] = $grade['grade'];
            $allSubjects[$grade['id_subjects']]['semester'][] = $grade['semestar'];
        }
        if ($allSubjects) {
            return $allSubjects;
        }
        return false;

    }

    /*
     *  Metod for displaying schedule!
     */
    public function getSchedule($id_sg)
    {
        $query = 'select sch.id_schedules, sch.day, sch.class, sch.type, sch.semester, sch.id_lecturer_subject, sbj.subject_title';
        $query .= ' from schedules as sch';
        $query .= ' join lecturer_subjects as ls on ls.id_lecturer_subject = sch.id_lecturer_subject';
        $query .= ' join subjects as sbj on ls.id_subject = sbj.id_subjects';
        $query .= ' join student_group as sg on sch.student_group_id = sg.id_student_group';
        $query .= ' where sg.id_student_group = :id_sg';
    
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_sg', $id_sg);
        $stmt->execute();
        $schedules = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $sg_schedule = [];
    
        foreach ($schedules as $schedule) {
            $sg_schedule[$schedule['id_schedules']] = $schedule;
        }
        if ($sg_schedule) {
            return $sg_schedule;
        }
        return false;
    }


    /*
     *  Metod for displaying messages and notifications!
     */
    public function getMessages($id_user)
    {
        
        $query = 'select *';
        $query .=' from(select m.id_messages, m.message, m.from_user, m.to_user, m.created_at, m.type, u.first_name, u.last_name';
        $query .=' from messages as m';
        $query .=' join users as u on m.to_user = u.id_user';
        $query .=' where u.id_user = :id_user)';
        $query .=' subquery join users as us on us.id_user = subquery.from_user';

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_user', $id_user);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {

            return $result;
        }
        return false;
    }
    /*
     *  Metod for saveing messages!
     */
    public function saveMessage($message, $to_user, $from_user, $type)
    {
        $query = 'insert into messages (message, from_user, to_user, type) values (:message, :to_user, :from_user, :type)';

        $stmt = $this->conn->prepare($query);
        var_dump($message);
        $stmt->bindParam(':message', $message);
        $stmt->bindParam(':from_user', $to_user);
        $stmt->bindParam(':to_user', $from_user);
        $stmt->bindParam(':type', $type);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function insertGrades($grade, $semestar, $id_user, $id_student, $id_subjects)
    {
    	$query = 'insert into grades (grade, semestar, id_student, id_user, id_subjects) values (:grade, :semestar, :id_student, :id_user, :id_subjects)';
		$stmt = $this->conn->prepare($query);
    	$stmt->bindParam(':grade', $grade);
    	$stmt->bindParam(':semestar', $semestar);
        $stmt->bindParam(':id_user', $id_user);
        $stmt->bindParam(':id_student', $id_student);
        $stmt->bindParam(':id_subjects', $id_subjects);
    	if ($stmt->execute()) {
    		return true;
    	}
    	return false;
    }

}