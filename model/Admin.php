<?php 

class Admin
{
	protected $conn = null;
	public function __construct($db)
	{
		$this->conn = $db;
	}
	/*
     * Method for check if  that email is free to use. Check if in db dont have that one! Admin  registration method
     */ 
    public function checkIfAvailable($email)
    {
    	$query = 'select * from users where email = :email';
		$stmt = $this->conn->prepare($query);
    	$stmt->bindParam(':email', $email);
    	$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		//if is not found return true (email not exists in db)
		if (!$result) {
			return true;			
		}
		return false;
    }
    /*
     * Method for saving into USERS table. Admin registration method
     */
    public function register($first_name, $last_name, $email, $password, $role_id)
    {
    	$query = 'insert into users  (first_name, last_name, email, password, role_id) values (:first_name, :last_name, :email, :password, :role_id)';
		$stmt = $this->conn->prepare($query);
    	$stmt->bindParam(':first_name', $first_name);
    	$stmt->bindParam(':last_name', $last_name);
    	$stmt->bindParam(':email', $email);
    	$stmt->bindParam(':password', $password);
    	$stmt->bindParam(':role_id', $role_id);
    	if ($stmt->execute()) {
    		return true;
    	}
    	return false;
    }
    /*
     * Method for saving into student table. Admin  registration method for students
     */
    public function studentRegistration($first_name, $last_name, $date_of_birth, $social_id, $student_group_id)
    {
        $query = 'insert into students (first_name, last_name, date_of_birth, social_id, student_group_id) values (:first_name, :last_name, :date_of_birth, :social_id, :student_group_id)';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':first_name', $first_name);
        $stmt->bindParam(':last_name', $last_name);
        $stmt->bindParam(':date_of_birth', $date_of_birth);
        $stmt->bindParam(':social_id', $social_id);
        $stmt->bindParam(':student_group_id', $student_group_id);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    /*
     * Method for returning all users in controller method  'all_us_st' on page all_us_st.
     */
    public function getAllUsers()
    {
        $query = 'select * from users ';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_user', $id_user);
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     *Method for returning all students in controller method  'all_us_st' on page all_us_st.
     */
    public function getAllStudents()
    {
        /***
         * kada radite neke query-je koji ne zavise od parametara ne morate da koristite prepare vec direktno izvrsite te upite
         * Kada radite sa prepare metodom, dva puta idete ka bazi, sto usporava load time. Naravno, kada postoje parametri, uvek radite
         * prepare, sigurnost je bitnija od brzine
         */
        $query = 'select * from students';
        $stmt = $this->conn->prepare($query);       
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for returning one user on controller method 'edit' on page edit.
     */
    public function getOneUser($id_user)
    {
        $query = 'select * from users where id_user = :id_user';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_user', $id_user);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for returning one studnet on controller method 'edit' on page edit.
     */
    public function getOneStudent($id_student)
    {
        $query = 'select * from students where id_student = :id_student';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_student', $id_student);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for saving (updateing) new user data into users table. controller method 'update' on page edit.
     */
    public function Update($id_user, $first_name, $last_name, $email, $password, $role_id)
    {
        $query = 'update users set first_name = :first_name, last_name = :last_name, email = :email, password = :password, role_id = :role_id where id_user = :id'; 
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':first_name', $first_name);
        $stmt->bindParam(':last_name', $last_name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':role_id', $role_id);
        $stmt->bindParam(':id', $id_user);

        /***
         * ili jos jednostavnije:
         * return $stmt->execute();
         */
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    /*
     * Method for saving (updateing) new student data into students table. controller method 'updateStudent' on page edit.
     */
    public function studentUpdate($id_student, $first_name, $last_name, $date_of_birth, $social_id, $student_group_id)
    {
        $query = 'update students set first_name = :first_name, last_name = :last_name, date_of_birth = :date_of_birth, social_id = :social_id, student_group_id = :student_group_id where id_student = :id';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':first_name', $first_name);
        $stmt->bindParam(':last_name', $last_name);
        $stmt->bindParam(':date_of_birth', $date_of_birth);
        $stmt->bindParam(':social_id', $social_id);
        $stmt->bindParam(':student_group_id', $student_group_id);
        $stmt->bindParam(':id', $id_student);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    /*
     * Method for deleting user and student from users, students tables. controller method 'edit' on page edit.
     */
    public function deleteUser($id_user)
    {
        /***
         * ja licno za ovakve upite uvek volim da dodam 'LIMIT 1', za svaki slucaj :)
         */
        $query = 'delete from users where id_user = :id_user';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_user', $id_user);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    /*
     *Method for returning all the subjects in controller method  'subjects' on page subjects.
     */
    public function getAllSubjects()
    {
        $query = 'select * from subjects';
        $stmt = $this->conn->prepare($query);       
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for returning one subject on controller method 'editsubject' on page subject.
     */
    public function getOneSubject($id_subjects)
    {
        $query = 'select * from subjects where id_subjects = :id_subjects';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_subjects', $id_subjects);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for saving (updating) new subject data into subjects table. controller method 'updatesubject' on page editsubject.
     */
    public function Update_subject($id_subjects, $subject_title)   
    {
        $query = 'update subjects set subject_title = :subject_title where id_subjects = :id_subjects'; 
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_subjects', $id_subjects);
        $stmt->bindParam(':subject_title', $subject_title);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     * Method for deleting subject from subjects table. controller method 'editsubject' on page editsubject.
     */
    public function deleteSubject($id_subjects)
    {
        $query = 'delete from subjects where id_subjects = :id_subjects';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_subjects', $id_subjects);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     * Method for checking if subject_title exists in db. 
     */ 
    public function checkIfExistsSubject($subject_title)
    {
    	$query = 'select * from subjects where subject_title = :subject_title';
		$stmt = $this->conn->prepare($query);
    	$stmt->bindParam(':subject_title', $subject_title);
    	$stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        //if the subject_title exists in the db, return true
        /***
         * ili jos jednostavnije
         * return $stmt->fetch(PDO::FETCH_ASSOC))
         */
		if ($result) {
			return true;			
		}
		return false;
    }

    /*
     * Method for saving a new subject into subjects table. Admin registration method for subjects
     */
    public function newSubject($subject_title)
    {
        $query = 'insert into subjects (subject_title) values (:subject_title)';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':subject_title', $subject_title);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     *Method for returning all the subgroups in controller method  'subgroups' on page subgroups.
     */
    public function getAllSubgroups()
    {
        $query = 'select * from subgroups';
        $stmt = $this->conn->prepare($query);       
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for returning one subgroup in controller method 'editsubgroup' on page subgroup.
     */
    public function getOneSubgroup($id_subgroup)
    {
        /***
         * kada koristite upite od kojih ocekujete iskljucivo jedan ili ni jedan rezultat, dodajte LIMIT 1,
         * na taj nacin ubrzavate proces jer ce sql cim nadje rezultat napustiti bazu i vratiti vam rezultat
         */
        $query = 'select * from subgroups where id_subgroup = :id_subgroup';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_subgroup', $id_subgroup);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for saving (updating) new subject data into subjects table. controller method 'updatesubject' on page editsubject.
     */
    public function Update_subgroup($id_subgroup, $subgroup_title)   
    {
        $query = 'update subgroups set subgroup_title = :subgroup_title where id_subgroup = :id_subgroup'; 
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_subgroup', $id_subgroup);
        $stmt->bindParam(':subgroup_title', $subgroup_title);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     * Method for deleting subject from subjects table. controller method 'editsubject' on page editsubject.
     */
    public function deleteSubgroup($id_subgroup)
    {
        $query = 'delete from subgroups where id_subgroup = :id_subgroup';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_subgroup', $id_subgroup);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     * Method for checking if subgroup_title exists in the db. 
     */
    public function checkIfExistsSubgroup($subgroup_title)
    {
    	$query = 'select * from subgroups where subgroup_title = :subgroup_title';
		$stmt = $this->conn->prepare($query);
    	$stmt->bindParam(':subgroup_title', $subgroup_title);
    	$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		//if there is a subgroup_title in the db, return true
		if ($result) {
			return true;			
		}
		return false;
    }
    /*
     * Method for saving a new subject into subjects table. Admin registration method for subjects
     */
    public function newSubgroup($subgroup_title)
    {
        $query = 'insert into subgroups (subgroup_title) values (:subgroup_title)';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':subgroup_title', $subgroup_title);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     *Method for returning all the studentgroups in controller method  'studentgroups' on page studentgroups.
     */
    public function getAllStudentgroups()
    {
        $query = 'select * from student_group';
        $stmt = $this->conn->prepare($query);       
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for returning one studentgroup in controller method 'editstudentgroup' on page studentgroup.
     */
    public function getOneStudentgroup($id_student_group)
    {
        $query = 'select * from student_group where id_student_group = :id_student_group';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_student_group', $id_student_group);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for saving (updating) new studentgroup data into studentgroups table. controller method 'updatestudentgroup' on page editstudentgroup.
     */
    public function Update_studentgroup($id_student_group, $start_year, $finish_year, $main_teacher_id, $id_subgroup, $id_years)   
    {
        $query = 'update student_group set start_year = :start_year, finish_year = :finish_year, main_teacher_id = :main_teacher_id, id_subgroup = :id_subgroup, id_years = :id_years where id_student_group = :id_student_group'; 
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_student_group', $id_student_group);
        $stmt->bindParam(':start_year', $start_year);
        $stmt->bindParam(':finish_year', $finish_year);
        $stmt->bindParam(':main_teacher_id', $main_teacher_id);
        $stmt->bindParam(':id_subgroup', $id_subgroup);
        $stmt->bindParam(':id_years', $id_years);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     * Method for deleting studentgroup from studentgroups table. controller method 'editstudentgroup' on page editstudentgroup.
     */
    public function deleteStudentgroup($id_student_group)
    {
        $query = 'delete from student_group where id_student_group = :id_student_group';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_student_group', $id_student_group);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    /*
     * Method for checking if studentgroup exists in the db.                    
     */

     
    public function checkIfExistsStudentgroup($id_subgroup, $id_years)
    {
    	$query = 'select * from student_group where id_subgroup = :id_subgroup and id_years = :id_years';
		$stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_subgroup', $id_subgroup);
        $stmt->bindParam(':id_years', $id_years);
    	$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		//if there are id_subgroup and id_years in the db, return true
		if ($result) {
			return true;			
		}
		return false;
    }  
    
    /*
     * Method for saving a new studentgroup into studentgroups table. Admin registration method for studentgroups
     */
    public function newStudentgroup($start_year, $finish_year, $main_teacher_id, $id_subgroup, $id_years)
    {
        $query = 'insert into student_group (start_year, finish_year, main_teacher_id, id_subgroup, id_years) values (:start_year, :finish_year, :main_teacher_id, :id_subgroup, :id_years)';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':start_year', $start_year);
        $stmt->bindParam(':finish_year', $finish_year);
        $stmt->bindParam(':main_teacher_id', $main_teacher_id);
        $stmt->bindParam(':id_subgroup', $id_subgroup);
        $stmt->bindParam(':id_years', $id_years);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

     /*
     *Method for returning schedule.  
     */
    public function getSchedule()    
    {
        $query = 'select day from schedules';
        $stmt = $this->conn->prepare($query);       
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    
       /*
     *  Metod for displaying schedule!
     */
    public function getScheduleByIdStudentGroup($id_sg)
    {
        $query = 'select sch.id_schedules, sch.day, sch.class, sch.type, sch.semester, sch.id_lecturer_subject, sbj.subject_title';
        $query .= ' from schedules as sch';
        $query .= ' join lecturer_subjects as ls on ls.id_lecturer_subject = sch.id_lecturer_subject';
        $query .= ' join subjects as sbj on ls.id_subject = sbj.id_subjects';
        $query .= ' join student_group as sg on sch.student_group_id = sg.id_student_group';
        $query .= ' where sg.id_student_group = :id_sg';
    
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_sg', $id_sg);
        $stmt->execute();
        $schedules = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $sg_schedule = [];
    
        foreach ($schedules as $schedule) {
            $sg_schedule[$schedule['id_schedules']] = $schedule;
        }
        if ($sg_schedule) {
            return $sg_schedule;
        }
        return false;
    }

     /*
     *Method for returning all from the lecturer_subjects table.
     */
    public function getlecturer_subjects()
    {
        $query = 'select id_lecturer_subject from lecturer_subjects order by id_lecturer_subject';

        $stmt = $this->conn->prepare($query);       
        $stmt->execute();
        $result = [];
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }

    /*
     *Method for inserting data into table schedules.
     */

    public function createSchedule($student_group_id, $day, $class, $semester, $type, $id_lecturer_subject)
    {
        $query = 'insert into schedules (student_group_id, day, class, semester, type, id_lecturer_subject) values (:id_student_group, :day, :class, :semester, :type, :id_lecturer_subject)';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_student_group', $student_group_id);
        $stmt->bindParam(':day', $day);
        $stmt->bindParam(':class', $class);
        $stmt->bindParam(':semester', $semester);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':id_lecturer_subject', $id_lecturer_subject);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

}
