<?php

class Parents
{
    private $conn = null;
    public function __construct($db)
    {
        $this->conn = $db;
    }
    /*
     * Method for returning data for student.
     */
    public function getStudent($id_user)
    {
        $query = 'select s.id_student, s.first_name, s.last_name, y.years_title, sbg.subgroup_title';
        $query .= ' from students as s ';
        $query .= ' join student_group as sg on s.student_group_id = sg.id_student_group';
        $query .= ' join years as y on sg.id_years = y.id_years';
        $query .= ' join subgroups as sbg on sg.id_subgroup = sbg.id_subgroup';
        $query .= ' join parent_student as ps on s.id_student = ps.student_id';
        $query .= ' join users as u on ps.parent_id = u.id_user';
        $query .= ' where u.id_user  = :id_user';

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_user', $id_user);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }
    /*
     * Method for returning data for main teacher.
     */
    public function getMainTeacher($id_student)
    {
        $query = 'select u.id_user, u.first_name, u.last_name, u.role_id';
        $query .= ' from users u ';
        $query .= ' join student_group sg on u.id_user = sg.main_teacher_id';
        $query .= ' join students s on s.student_group_id = sg.id_student_group';
        $query .= ' where s.id_student = :id_student';

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id_student', $id_student);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;
    }

}
