<?php 
session_start();
define("WEBROOT", "http://www.newdnevnik.com");

/***
 * Izbegavajte ucitavanje svih klasa, potrebno je ucitati samo klase koje se koriste u jednom request-u.
 * Plus su require_once f-je jako spore, uvek je bolje koristiti require f-ju i voditi racuna sta se gde i kada ucitava
 * Bolje je napraviti autoloader za klase. Izguglajte o autoloaderima u php-u i funkciji spl_autoload_register.
 * Obzirom da radite na malo kompleksnijem projektu koji koristi MVC, bitno je i da naucite sve o namespace-ovima koji ce vam biti 
 * potrebni za autoloader.
 */
foreach (glob('./classes/*') as $class_name) {
	require_once($class_name);                     
}
foreach (glob('./model/*') as $model_name) {
	require_once($model_name);
}
foreach (glob('./controller/*') as $controller_name) {
	require_once($controller_name);
}
// instansiate db class
require_once('./Database.php');
$database = new Database();
// connecting with db;
$conn = $database->connect();

$request = new Request();
$router = new Router($request);
