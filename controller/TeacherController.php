<?php 


class TeacherController
{
 
    public $grade = '';
    public $semestar = '';
    public $id_student = '';
    public $id_user = '';
    public $id_subjects = '';
    public $to_user = '';
    public $from_user = '';
    public $type = '';
    public $message = '';

	private $err = 'Something went wrong, please try again';

    public function login()
    {
        View::load('teacher', 'teacherpage');
    }
	public function teacherpage()
	{
        global $conn;
        
        $teacher = new Teacher($conn);
        //get the classroom for the teacher
        $_SESSION['classroom'] = $teacher->getClassroom($_SESSION['user']['id_user']);
        $_SESSION['sg_schedule'] = $teacher->getSchedule($_SESSION['classroom']['id_student_group']);
        $_SESSION['new_schedule'] = $this->getSortedSchedule();
		View::load('teacher', 'teacherpage');
	}

    protected function getSortedSchedule()
    {
        if(isset($_SESSION['sg_schedule']) && $_SESSION['sg_schedule'] !== false){
            $schedule = $_SESSION['sg_schedule'];
            $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
            $sorted = [];

            foreach ($days as $day) {
                for ($i = 1; $i <= 7 ; $i++) {
                    foreach ($schedule as $cas) {
                        if ($cas['day'] == $day && $cas['class'] == $i) {
                            $sorted[$i][$day] = $cas['subject_title'];
            View::$data = $sorted; 
                        }
                    }
                }
            }
        }
        // var_dump(View::$data);
        
    }

    public function logout()
    {
        unset($_SESSION['rola']);
        header('Location:http://www.newdnevnik.com/login/login');
    }


    public function studentgroup()
    {
        global $conn;
        $studentgroup = new Teacher($conn);
        //get allstudents from same classroom
        $_SESSION['allstudents'] = [];
        $_SESSION['allstudents'] = $studentgroup->getAllstudents($_SESSION['user']['id_user']);
        View::load('teacher', 'studentgroup');
    }


    public function studentinfo()
    {
        global $conn;

        $studentinfo = new Teacher($conn);
        //get all subjects for one grade sending student id from GET!
        $_SESSION['allSubjects'] = [];
        $_SESSION['allSubjects'] = $studentinfo->getSubjects($_GET['id']);
        View::load('teacher', 'studentinfo');
    } 

   public function insertGrades(){   
        var_dump($_POST);

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $this->grade = $_POST['grade'];
            $this->semestar = $_POST['semestar'];
            $this->id_user = $_SESSION['user']['id_user'];
            $this->id_student = $_POST['id_student'];
            $this->id_subjects = $_POST['subject'];

            if (!empty($this->error)) {
                $errors = '?';
                $lastKey = $this->_array_key_last($this->error);
                foreach ($this->error as $error => $msg) {
                    if ($error == $lastKey) {
                        $errors .= $error . '=' . $msg;
                    } else {
                        $errors .= $error . '=' . $msg . '&';
                    }
                }
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            } else {

           global $conn;
           $insert_grades = new Teacher($conn);
           $is_created = $insert_grades->insertGrades($this->grade, $this->semestar, $this->id_user, $this->id_student, $this->id_subjects);
            
              if($is_created){
              $msg = 'Uspesno ste uneli ocenu!';
              header('Location: '. "/teacher/studentinfo?id=4 ".'&success=' . $msg);
              }else{
              $msg = 'Pokusajte ponovo, nesto je pogresno!';
              header('Location: '. "/teacher/studentinfo?id=4 ".'&error=' . $msg);
              }

         
            }
        } 
    }  


    public function teachernotification()
    {
        global $conn;
        $teachernotification = new Teacher($conn);
        //gets all maessages and notification for one user!
//        $_SESSION['messages'] = [];
        $_SESSION['messages'] = $teachernotification->getMessages($_SESSION['user']['id_user']);
        View::load('teacher', 'teachernotification');
    }
    public function sendMessage()
    {
        global $conn;
        $send_message = new Teacher($conn);
        var_dump($_POST);
        
        if (isset($_POST)) {
            $this->to_user = $_POST['to_user'];
            $this->from_user = $_POST['from_user'];
            $this->type = $_POST['type'];
            $this->message = $_POST['msg'];

            if (empty($this->message)) {
               $this->error['msg'] = 'Polje ne sme biti prazno!';
            }
            $is_send = $send_message->saveMessage($this->message, $this->from_user, $this->to_user, $this->type);
                if($is_send){
                    $msg = 'Uspesno ste poslali poruku!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
                } else{
                    $msg = 'Pokusajte ponovo, nesto je pogresno!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                }
            }
    } 


}

