<?php
class AdminController
{
    //    users params (data)
    /***
     * Ovo su podaci koji pripadaju korisniku, odeljenju itd itd
     * Oni ne bi trebali da budu deo kontrolera, kontroler je samo tu da hendluje zahtev
     * Modeli su ti koji trebaju da sadrze ove parametre i sa njima treba da se radi
     */
	public $first_name = '';
    public $last_name = '';
    public $email = '';
    public $password = '';
    public $re_password = '';
    public $role_id = '';
    public $registrate = '';
    public $error = [];
    public $date_of_birth = '';
    public $social_id = '';
    public $student_group_id = '';
    public $subject_title = '';
    public $subgroup_title = '';
    public $start_year = '';
    public $finish_year = '';
    public $main_teacher_id = '';
    public $id_subgroup = '';
    public $id_years = '';
    public $studentgroup = '';
    // public $id = '';

    /*
     * Method for displaying adminpage! (instantiate adminpage page);
     */
	public function adminpage()
	{
		View::load('admin', 'adminpage');
	}
	/*
	 * Method for logout user! just unset session[rola];
     */
    public function logout()
    {
        /***
         * Ovo je metoda koja se koristi u svakom kontroleru, don't repeat yourself :)
         * Mozda bi mogli da napravite neki bazni kontoler kog ce svi ostali nasledjivati,
         * taj metod bi za pocetak mogao da ima logout metodu
         */
        unset($_SESSION['rola']);
        header('Location:/login/login');
    }
    /*
	 * Method for creating new user! (instantiate newuserform page);
     */
    public function newuserform()
    {
    	View::load('admin', 'newuserform');
    }
    /*
	 * Method for creating new student! (instantiate newstudentform page);
     */
    public function newstudentform()
    {
        global $conn;
        $newstudentform = new Admin($conn); 
        View::$data['all_studentgroups'] = $newstudentform->getAllStudentgroups();
    	View::load('admin', 'newstudentform');
    }
    
    /*
     * Method for displaying all users and students trough array $data!;
     */
    public function all_us_st()
    {
        global $conn;
        $usersstudents = new Admin($conn);
        View::$data['all_users'] = $usersstudents->getAllUsers();
        // $_SESSION['all_students'] = $usersstudents->getAllStudents();
        View::$data['all_students'] = $usersstudents->getAllStudents();
        View::load('admin', 'all_us_st');
    }
    /*
     * Method for returning data for one user in array $data. and deleting user from db! (instantiate edit page);
     */
    public function edit()
    {
        global $conn;
        $oneuser = new Admin($conn);
        /***
         * Izbegavajte da konstantno radite sa View::$data nizom, mozda bi bolje resenje bilo da se na kraju metode
         * popuni taj niz promeljivima
         */
        View::$data['all_studentgroups'] = $oneuser->getAllStudentgroups();

        if(isset($_GET['id'])){
            View::$data['one_user'] = $oneuser->getOneUser($_GET['id']);
        }
        if(isset($_GET['id_student'])){
            View::$data['one_student'] = $oneuser->getOneStudent($_GET['id_student']);
        }

        if (isset($_GET['delete'])) {
            if ($id = $oneuser->deleteUser($_GET['delete'])) {
                $msg = 'Successfully deleted!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
            }else{
                $msg = 'You did something wrong!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            }
        }
        View::load('admin', 'edit');
    }
    /*
     * Method for checking data from edit form on edit page and update user;
     */
    public function update()
    {
        global $conn;
        $user = new Admin($conn);
        $user_data = $user->getOneUser($_POST['id_user']);

        if (isset($_POST)) {
            /***
             * Sustina OOP-a je razbijanje velikog problema na male celine,
             * ukoliko postoje vise logicki grupisanih radnji koje zelis da izvrsis, uvek probaj da ih razbijes na manje celine
             * u slucaju ispod:
             * -napravis metodu kojoj se salje $_POST niz i koja popunjava User model
             * -napravis metodu koja trimuje sve podatke
             * -napravis metodu koja radi sve ove regularne izraze
             * Naravno, sve te metode bi trebale da se nalaze u modelu.
             * Isto je i za ostale metode klase
             */
            $this->first_name = $_POST['first_name'];
            $this->last_name = $_POST['last_name'];
            $this->email = $_POST['email'];
            $this->password = $_POST['password'];
            $this->role_id = isset($_POST['role_id']) ? $_POST['role_id'] : $user_data[0]['role_id'];
            $this->id_user = $_POST['id_user'];

            //Remove any excess whitespace
            $this->first_name = trim($this->first_name);
            $this->last_name = trim($this->last_name);
            $this->email = trim($this->email);
            $this->password = trim($this->password);
            $this->role_id = trim($this->role_id);

            //Check that the input values are of the proper format (REGEX!)      // PROVERI DA LI OVAJ DEO KODA TREBA!!!!!!!!!!!!
            if (!preg_match('/^[a-zA-Z]+$/', $this->first_name)) {
                $this->error['first_name'] = 'Uneti ponovo ime!';
            }
            if (!preg_match('/^[a-zA-Z]+$/', $this->last_name)) {
                $this->error['last_name'] = 'Uneti ponovo prezime!';
            }
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->error['email'] = 'Email adresa nije validna!';
            }
            if (!preg_match('/^[1-9][0-9]*$/', $this->role_id) && filter_var($this->role_id, FILTER_VALIDATE_INT)) {
                $this->error['role_id'] = 'Uneti ponovo prezime!';
            }

            //if property is empty adding data from array $user_data where is every open user have key 0!
            if (empty($this->first_name)) {
               $this->first_name = $user_data[0]['first_name'];
            }
            if (empty($this->last_name)) {
                $this->last_name = $user_data[0]['last_name'];
            }
            if (empty($this->email)) {
                $this->email = $user_data[0]['email'];
            }
            if (empty($this->password)) {
                $this->password = $user_data[0]['password'];
            }    

            // creates a new password hash, Use the md5 algorithm
            $enc_pass = md5($this->password);

            // checking if email exists in db.
            $isEmailAvailable = true;
            if ($_POST['email'] && ($_POST['email'] != $user_data[0]['email'])) {
                $isEmailAvailable = $user->checkIfAvailable($this->email);
                if (!$isEmailAvailable) {                    
                    $msg = 'Email je vec u bazi podataka';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'&error=' . $msg);
                }
            }
            if ($isEmailAvailable) {
                if ($user->Update($this->id_user,$this->first_name,$this->last_name,$this->email, $enc_pass, $this->role_id)){
                    $msg = 'Uspesno ste apdejtovali!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'&success=' . $msg);
                } else{
                    $msg = 'Pokusajte ponovo, nesto je pogresno!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'&error=' . $msg);
                }
            }
            
        }
    }

    /*
     * Method for checking data from edit form on edit page and update user;
     */
    public function updateStudent()
    {
        global $conn;
        $student = new Admin($conn);
        $student_data = $student->getOneStudent($_POST['id_student']);
        var_dump($_POST);
        var_dump($student_data);

        if (isset($_POST)) {
            $this->first_name = $_POST['first_name'];
            $this->last_name = $_POST['last_name'];
            $this->date_of_birth = $_POST['date_of_birth'];
            $this->social_id = $_POST['social_id'];
            $this->student_group_id = isset($_POST['student_group_id']) ? $_POST['student_group_id'] : $student_data[0]['student_group_id'];
            $this->id_student = $_POST['id_student'];

            //Remove any excess whitespace
            $this->first_name = trim($this->first_name);
            $this->last_name = trim($this->last_name);
            $this->date_of_birth = trim($this->date_of_birth);
            $this->social_id = trim($this->social_id);
            $this->student_group_id = trim($this->student_group_id);

            //Check that the input values are of the proper format (REGEX!)     // PROVERI DA LI OVAJ DEO KODA TREBA!!!!!!!!!!!!
            if (!preg_match('/^[a-zA-Z]+$/', $this->first_name)) {
                $this->error['first_name'] = 'Uneti ponovo ime!';
            }
            if (!preg_match('/^[a-zA-Z]+$/', $this->last_name)) {
                $this->error['last_name'] = 'Uneti ponovo prezime!';
            }
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->error['email'] = 'Email adresa nije validna!';
            }
            if (!preg_match('/^[1-9][0-9]*$/', $this->role_id) && filter_var($this->role_id, FILTER_VALIDATE_INT)) {
                $this->error['role_id'] = 'Uneti ponovo prezime!';
            }

            //if property is empty adding data from array $user_data where is every open user have key 0!
            if (empty($this->first_name)) {
               $this->first_name = $student_data[0]['first_name'];
            }
            if (empty($this->last_name)) {
                $this->last_name = $student_data[0]['last_name'];
            }
            if (empty($this->date_of_birth)) {
                $this->date_of_birth = $student_data[0]['date_of_birth'];
            }
            if (empty($this->social_id)) {
                $this->social_id = $student_data[0]['social_id'];
            } 
            if (empty($this->student_group_id)) {
                $this->student_group_id = $student_data[0]['student_group_id'];
            }
            //Check if social id is a proper long.
            if(strlen($this->social_id) < 13 || strlen($this->social_id) > 13) {
                $msg = 'Uneti tacno 13 karaktera!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            }
            
            if ($student->studentUpdate($this->id_student,$this->first_name,$this->last_name,$this->date_of_birth, $this->social_id, $this->student_group_id)){
                $msg = 'Student updated!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&success=' . $msg);
            } else{
                $msg = 'Pokusajte ponovo, nesto je pogresno!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&error=' . $msg);
            }
        }
    }

    /*
     * Method for validating  data from USER registration form and sending to admin Model (NEW USER)!
     */
    public function registration()
    {
        if (isset($_POST)) {
            $this->first_name = $_POST['first_name'];
            $this->last_name = $_POST['last_name'];
            $this->email = $_POST['email'];
            $this->password = $_POST['password'];
            $this->re_password = $_POST['re_password'];
            $this->role_id = $_POST['role_id'];
            var_dump($this->role_id);

            //Remove any excess whitespace
            $this->first_name = trim($this->first_name);
            $this->last_name = trim($this->last_name);
            $this->email = trim($this->email);
            $this->password = trim($this->password);
            $this->re_password = trim($this->re_password);
            $this->role_id = trim($this->role_id);

            //Check that the input values are of the proper format (REGEX!)
            if (!preg_match('/^[a-zA-Z]+$/', $this->first_name)) {
                $this->error['first_name'] = 'Uneti ponovo ime!';
            }
            if (!preg_match('/^[a-zA-Z]+$/', $this->last_name)) {
                $this->error['last_name'] = 'Uneti ponovo prezime!';
            }
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->error['email'] = 'Email adresa nije validna!';
            }
            if (!preg_match('/^[1-9][0-9]*$/', $this->role_id) && filter_var($this->role_id, FILTER_VALIDATE_INT)) {
                $this->error['role_id'] = 'Uneti ponovo prezime!';
            }
            if (empty($this->first_name)) {
               $this->error['first_name'] = 'Polje ne sme biti prazno!';
            }
            if (empty($this->last_name)) {
                $this->error['last_name'] = 'Polje ne sme biti prazno!';
            }
            if (empty($this->email)) {
                $this->error['email'] = 'Polje ne sme biti prazno!';
            }
            if (empty($this->role_id)) {
                $this->error['role_id'] = 'Polje ne sme biti prazno!';
            }
            if (empty($this->password)) {
                $this->error['password'] = 'Polje ne sme biti prazno!';
            }
            if ($this->re_password != $this->password || empty($this->re_password)) {
                $this->error['password'] = 'Sifre se ne poklapaju!';
            }
            // //if have errors adds ?, if there only one err add =error + msg, if have more errs adding & after each err msg.
            if (!empty($this->error)) {
                $errors = '?';
                $lastKey = $this->_array_key_last($this->error);
                foreach ($this->error as $error => $msg) {
                    if ($error == $lastKey) {
                        $errors .= $error . '=' . $msg;
                    } else {
                        $errors .= $error . '=' . $msg . '&';
                    }
                }
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            } else {
                // creates a new password hash, Use the md5 algorithm
                $enc_pass = md5($this->password);
                global $conn;
                $user = new Admin($conn);
                // checking if email exists in db.
                if ($user->checkIfAvailable($this->email)) {
                    $is_created = $user->register($this->first_name,$this->last_name,$this->email, $enc_pass, $this->role_id);
                    if($is_created){
                        $msg = 'Uspesno ste se registrovali!';
                        header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
                    } else{
                        $msg = 'Pokusajte ponovo, nesto je pogresno!';
                        header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                     }
                } else{
                    $msg = 'Email adresa vec postoji, pokusajte sa drugom';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                }
            }
        }
    }
    /*
    *   Function for finding last key in an array.
    */ 
    private function _array_key_last(array $array){
        return (!empty($array)) ? array_keys($array)[count($array)-1] : null;
    }
    /*
     * Method for validating  data from STUDENT registration form and sending to admin Model (NEW STUDENT)!
     */
    public function studentRegistration()
    {
        var_dump($_POST);
        if (isset($_POST)) {
            $this->first_name = $_POST['first_name'];
            $this->last_name = $_POST['last_name'];
            $this->date_of_birth = $_POST['date_of_birth'];
            $this->social_id = $_POST['social_id'];
            $this->student_group_id = $_POST['student_group_id'];

            //Remove any excess whitespace
            $this->first_name = trim($this->first_name);
            $this->last_name = trim($this->last_name);
            $this->date_of_birth = trim($this->date_of_birth);
            $this->social_id = trim($this->social_id);
            $this->student_group_id = trim($this->student_group_id);
            //Check if social id is a proper long.
            if(strlen($this->social_id) < 13 || strlen($this->social_id) > 13) {
                $msg = 'Uneti tacno 13 karaktera!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            }
            //Check that the input values are of the proper format
            if (!preg_match('/^[a-zA-Z]+$/', $this->first_name)) {
                $this->error['first_name'] = 'Uneti ponovo ime!';
            }
            if (!preg_match('/^[a-zA-Z]+$/', $this->last_name)) {
                $this->error['last_name'] = 'Uneti ponovo prezime!';
            }
            if (preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $this->date_of_birth, $matches)) {
                $this->error['date_of_birth'] = 'Molim vas unesite odgovarajuci format - dd/mm/yyyy';
            }
            if (preg_match("/^(\([\d]{3}\)|[\d]{3})?(-|\s*)?([\d]{3})?(-|\s*)?[\d]{3}(-|\s*)?[\d]{3}$/", $this->social_id)) {
                $this->error['social_id'] = 'Molim vas unesite odgovarajuci broj karaktera';
            }
            if (!preg_match('/^[1-9][0-9]*$/', $this->student_group_id) && filter_var($this->student_group_id, FILTER_VALIDATE_INT)) {
                $this->error['student_group_id'] = 'Uneti ponovo studentsku grupu!';
            }

            if (empty($this->first_name)) {
                $this->error['first_name'] = 'Polje ime ne sme biti prazno!';
            }
            if (empty($this->last_name)) {
                $this->error['last_name'] = 'Polje prezime ne sme biti prazno!';
            }
            if (empty($this->date_of_birth)) {
                $this->error['date_of_birth'] = 'Polje datum ne sme biti prazno!';
            }
            var_dump($this->date_of_birth);
            if (empty($this->social_id)) {
                $this->error['social_id'] = 'Polje jmbg ne sme biti prazno!';
            }
            if (empty($this->student_group_id)) {
                $this->error['student_group_id'] = 'Polje odeljenje ne sme biti prazno!';
            }
            // //if have errors adds ?, if there only one err add =error + msg, if have more errs adding & after each err msg.
            if (!empty($this->error)) {
                $errors = '?';
                $lastKey = $this->_array_key_last($this->error);
                foreach ($this->error as $error => $msg) {
                    if ($error == $lastKey) {
                        $errors .= $error . '=' . $msg;
                    } else {
                        $errors .= $error . '=' . $msg . '&';
                    }
                }
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            } else {

                global $conn;
                $student = new Admin($conn);
              //View::$data['all_studentgroups'] = $student->getAllStudentgroups();
                $is_created = $student->studentRegistration($this->first_name, $this->last_name, $this->date_of_birth, $this->social_id, $this->student_group_id);
                if($is_created){
                    $msg = 'Uspesno ste registrovali studenta!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
                } else{
                    $msg = 'Pokusajte ponovo, nesto je pogresno!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                }
            }
        }
    }
    /*
     * Method for displaying all the subjects through array $data!;
     */
    public function subjects()
    {
        global $conn;
        $subjects = new Admin($conn);
        View::$data['all_subjects'] = $subjects->getAllSubjects();
        // $_SESSION['subjects'] = $usersstudents->getAllStudents();
        View::load('admin', 'subjects');
    }
    /*
     * Method for returning data for one subject in array $data and deleting subject from db! (instantiate editsubject page);
     */
    public function editsubject()
    {
        global $conn;
        $subject = new Admin($conn);
        if(isset($_GET['id'])){
            View::$data['one_subject'] = $subject->getOneSubject($_GET['id']);
        }

        if (isset($_GET['delete'])) {
            if ($id_subjects = $subject->deleteSubject($_GET['delete'])) {
                $msg = 'Successfully deleted!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
            }else{
                $msg = 'You did something wrong!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            }
        }
        View::load('admin', 'editsubject');
    }
    
    /*
     * Method for checking data from editsubject form on editsubject page and update subject;
     */
    public function updatesubject()
    {
        global $conn;
        $subject = new Admin($conn);
        $subject_data = $subject->getOneSubject($_POST['id_subjects']);
        

        if (isset($_POST)) {
            $this->id_subjects = $_POST['id_subjects']; 
            $this->subject_title = $_POST['subject_title'];
            //Remove any excess whitespace
            $this->subject_title = trim($this->subject_title);
            var_dump($this->subject_title); 

            //if property is empty adding data from array $user_data where is every open user have key 0!
            if (empty($this->subject_title)) {
               $this->subject_title = $subject_data[0]['subject_title'];
            }  
            
            if ($subject->Update_subject($this->id_subjects,$this->subject_title)){
                $msg = 'Uspesno ste updejtovali predmet!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&success=' . $msg);
            } else{
                $msg = 'Pokusajte ponovo, nesto je pogresno!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&error=' . $msg);
            }
        }
    }
    /*
	 * Method for creating new subject! (instantiate newsubjectform page);
     */
    public function newsubjectform()
    {
    	View::load('admin', 'newsubjectform');
    }
    /*
     * Method for validating data from new subject form and sending them to admin Model (NEW SUBJECT)!
     */
    public function newsubject()
    {
        
        if (isset($_POST)) {
            $this->subject_title = $_POST['subject_title'];

            //Remove any excess whitespace
            $this->subject_title = trim($this->subject_title);
            
            
            //Check that the input values are of the proper format
            if (!preg_match('/^[a-zA-Z1-9]+$/', $this->subject_title)) {
                $this->error['subject_title'] = 'Uneti ponovo predmet!';
            }
            

            if (empty($this->subject_title)) {
                $this->error['subject_title'] = 'Polje ne sme biti prazno!';
            }
            
            
            //if there are some errors add ?, if there's only one err add =error + msg, if there are more errs add & after each err msg.
            if (!empty($this->error)) {
                $errors = '?';
                $lastKey = $this->_array_key_last($this->error);
                foreach ($this->error as $error => $msg) {
                    if ($error == $lastKey) {
                        $errors .= $error . '=' . $msg;
                    } else {
                        $errors .= $error . '=' . $msg . '&';
                    }
                }
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            } else {
                global $conn;
                $subject = new Admin($conn);
                //if the new subject_title isn't already in the db, it will be created through this method and vice versa
                if (!$subject->checkIfExistsSubject($this->subject_title)) {
                  $is_created = $subject->newSubject($this->subject_title);
                  if($is_created){
                    $msg = 'Uspesno ste kreirali novi predmet!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
                  } else{
                    $msg = 'Pokusajte ponovo, nesto je pogresno!';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                  }
                }else{
                    $msg = 'Predmet vec postoji, unesite drugi naziv';
                    header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                }
            }
        }
    }

    /*
     * Method for displaying all the subgroups through array $data!;
     */
    public function subgroups()
    {
        global $conn;
        $subgroups = new Admin($conn);
        View::$data['all_subgroups'] = $subgroups->getAllSubgroups();
        // $_SESSION['subjects'] = $usersstudents->getAllStudents();
        View::load('admin', 'subgroups');
    }
    /*
     * Method for returning data for one subgroup in array $data and deleting subgroup from db! (instantiate editsubgroup page);
     */
    public function editsubgroup()
    {
        global $conn;
        $subgroup = new Admin($conn);
        if(isset($_GET['id'])){
            View::$data['one_subgroup'] = $subgroup->getOneSubgroup($_GET['id']);
        }

        if (isset($_GET['delete'])) {
            if ($id_subgroup = $subgroup->deleteSubgroup($_GET['delete'])) {
                $msg = 'Successfully deleted!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
            }else{
                $msg = 'You did something wrong!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            }
        }
        View::load('admin', 'editsubgroup');
    }
    
    /*
     * Method for checking data from editsubgroup form on editsubgroup page and update subgroup;
     */
    public function updatesubgroup()
    {
        global $conn;
        $subgroup = new Admin($conn);
        $subgroup_data = $subgroup->getOneSubgroup($_POST['id_subgroup']);
        

        if (isset($_POST)) {
            $this->id_subgroup = $_POST['id_subgroup']; 
            $this->subgroup_title = $_POST['subgroup_title'];
            //Remove any excess whitespace
            $this->subgroup_title = trim($this->subgroup_title);
            var_dump($this->subgroup_title); 

            //if property is empty adding data from array $user_data where is every open user have key 0!
            if (empty($this->subgroup_title)) {
               $this->subgroup_title = $subgroup_data[0]['subgroup_title'];
            }  
            
            if ($subgroup->Update_subgroup($this->id_subgroup,$this->subgroup_title)){
                $msg = 'Uspesno ste updejtovali odeljenje!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&success=' . $msg);
            } else{
                $msg = 'Pokusajte ponovo, nesto je pogresno!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&error=' . $msg);
            }
        }
    }
    /*
	 * Method for creating new subject! (instantiate newsubgroupform page);
     */
    public function newsubgroupform()
    {
    	View::load('admin', 'newsubgroupform');
    }
    /*
     * Method for validating data from new subgroup form and sending them to admin Model (NEW SUBJECT)!
     */
    public function newsubgroup()
    {
        
        if (isset($_POST)) {
            $this->subgroup_title = $_POST['subgroup_title'];

            //Remove any excess whitespace
            $this->subgroup_title = trim($this->subgroup_title);
            
            
            //Check that the input values are of the proper format
            if (!preg_match('/^[1-9]+$/', $this->subgroup_title)) {
                $this->error['subgroup_title'] = 'Uneti ponovo odeljenje!';
            }
            

            if (empty($this->subgroup_title)) {
                $this->error['subgroup_title'] = 'Polje ne sme biti prazno!';
            }
            
            
            //if there are some errors add ?, if there's only one err add =error + msg, if there are more errs add & after each err msg.
            if (!empty($this->error)) {
                $errors = '?';
                $lastKey = $this->_array_key_last($this->error);
                foreach ($this->error as $error => $msg) {
                    if ($error == $lastKey) {
                        $errors .= $error . '=' . $msg;
                    } else {
                        $errors .= $error . '=' . $msg . '&';
                    }
                }
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            } else {
                global $conn;
                $subgroup = new Admin($conn);
                //if the new subgroup_title isn't already in the db, it will be created through this method and vice versa
                if (!$subgroup->checkIfExistsSubgroup($this->subgroup_title)) {
                    $is_created = $subgroup->newSubgroup($this->subgroup_title);
                    if($is_created){
                      $msg = 'Uspesno ste kreirali novo odeljenje!';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
                    } else{
                      $msg = 'Pokusajte ponovo, nesto je pogresno!';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                    }
                  }else{
                      $msg = 'Odeljenje vec postoji, unesite drugi naziv';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                  }
            }
        }
    }

     /*
     * Method for displaying all the studentgroups through array $data!;
     */
    public function studentgroups()
    {
        global $conn;
        $studentgroups = new Admin($conn);
        View::$data['all_studentgroups'] = $studentgroups->getAllStudentgroups();
        // $_SESSION['studentgroups'] = $studentgroups->getAllStudentgroups();
        View::load('admin', 'studentgroups'); 
    }
    /*
     * Method for returning data for one studentgroup in array $data and deleting studentgroup from db! (instantiate editsubject page);
     */
    public function editstudentgroup()
    {
        global $conn;
        $studentgroup = new Admin($conn);
        if(isset($_GET['id'])){
            View::$data['one_studentgroup'] = $studentgroup->getOneStudentgroup($_GET['id']);
        }

        if (isset($_GET['delete'])) {
            if ($id_studentgroup = $studentgroup->deleteStudentgroup($_GET['delete'])) {
                $msg = 'Successfully deleted!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
            }else{
                $msg = 'You did something wrong!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            }
        }
        View::load('admin', 'editstudentgroup');
    }
    
    /*
     * Method for checking data from editsubject form on editsubject page and update subject;
     */
    public function updatestudentgroup()
    {
        global $conn;
        $studentgroup = new Admin($conn);
        $studentgroup_data = $studentgroup->getOneStudentgroup($_POST['id_studentgroup']);  //u editstudentgroup.php-u je name="id_studentgroup", a ne "id_student_group", POST je od tog name-a
    
        if (isset($_POST)) {
            $this->start_year = $_POST['start_year'];
            $this->finish_year = $_POST['finish_year'];
            $this->main_teacher_id = $_POST['main_teacher_id'];
            $this->id_subgroup = $_POST['id_subgroup'];
            $this->id_years = $_POST['id_years'];
            var_dump($_POST);
            var_dump($studentgroup_data);

            //Removes any excess whitespace
            $this->start_year = trim($this->start_year);
            $this->finish_year = trim($this->finish_year);
            $this->main_teacher_id = trim($this->main_teacher_id);
            $this->id_subgroup = trim($this->id_subgroup);
            $this->id_years = trim($this->id_years);
            

            //if property is empty adding data from array $user_data where every open user has key 0!
            if (empty($this->start_year)) {
               $this->start_year = $studentgroup_data[0]['start_year'];
            } 
            if (empty($this->finish_year)) {
                $this->finish_year = $studentgroup_data[0]['finish_year'];
            }
            if (empty($this->main_tacher_id)) {
                $this->main_teacher_id = $studentgroup_data[0]['main_teacher_id'];
            } 
            if (empty($this->id_subgroup)) {
                $this->id_subgroup = $studentgroup_data[0]['id_subgroup'];
            } 
            if (empty($this->id_years)) {
                $this->id_years = $studentgroup_data[0]['id_years'];
            }  

            if ($studentgroup->Update_studentgroup($this->id_student_group,$this->start_year,$this->finish_year,$this->main_teacher_id,$this->id_subgroup,$this->id_years)){
                $msg = 'Uspesno ste updejtovali odeljenje!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&success=' . $msg);
            } else{
                $msg = 'Pokusajte ponovo, nesto je pogresno!';
                header('Location: '. $_SERVER['HTTP_REFERER'] .'&error=' . $msg);
            }
        }
    }
    /*
	 * Method for creating new studentgroup! (instantiate studentgroupform page);
     */
    public function newstudentgroupform()
    {
    	View::load('admin', 'newstudentgroupform');
    }
    /*
     * Method for validating data from newstudentgroup form and sending them to admin Model (NEW student group)!
     */
    public function newstudentgroup()
    {
        
        if (isset($_POST)) {
            $this->start_year = $_POST['start_year'];
            $this->finish_year = $_POST['finish_year'];
            $this->main_teacher_id = $_POST['main_teacher_id'];
            $this->id_subgroup = $_POST['id_subgroup'];
            $this->id_years = $_POST['id_years'];

            //Remove any excess whitespace
            $this->start_year = trim($this->start_year);
            $this->finish_year = trim($this->finish_year);
            $this->main_teacher_id = trim($this->main_teacher_id);
            $this->id_subgroup = trim($this->id_subgroup);
            $this->id_years = trim($this->id_years);
            
            
            //Check that the input values are of the proper format
/*
            if (!preg_match('/^[1-9]+$/', $this->start_year)) {
                $this->error['start_year'] = 'Uneti ponovo godinu početka!';
            } 
            if (!preg_match('/^[1-9]+$/', $this->finish_year)) {
                $this->error['finish_year'] = 'Uneti ponovo godinu završetka!';
            } 
            if (!preg_match('/^[1-9]+$/', $this->main_teacher_id)) {
                $this->error['main_teacher_id'] = 'Uneti ponovo id učitelja!';
            }
            if (!preg_match('/^[1-9]+$/', $this->id_subgroup)) {
                $this->error['id_subgroup'] = 'Uneti ponovo id odeljenja!';
            }
            if (!preg_match('/^[1-8]+$/', $this->id_years)) {
                $this->error['id_years'] = 'Uneti ponovo id razreda!';
            }    */
            

            if (empty($this->start_year)) {
                $this->error['start_year'] = 'Polje ne sme biti prazno!';
            }
            if (empty($this->finish_year)) {
                $this->error['finish_year'] = 'Polje ne sme biti prazno!';
            }
            if (empty($this->main_teacher_id)) {
                $this->error['main_teacher_id'] = 'Polje ne sme biti prazno!';
            } 
            if (empty($this->id_subgroup)) {
                $this->error['id_subgroup'] = 'Polje ne sme biti prazno!';
            } 
            if (empty($this->id_years)) {
                $this->error['id_years'] = 'Polje ne sme biti prazno!';
            } 
            
            
            //if there are some errors add ?, if there's only one err add =error + msg, if there are more errs add & after each err msg.
            if (!empty($this->error)) {
                $errors = '?';
                $lastKey = $this->_array_key_last($this->error);
                foreach ($this->error as $error => $msg) {
                    if ($error == $lastKey) {
                        $errors .= $error . '=' . $msg;
                    } else {
                        $errors .= $error . '=' . $msg . '&';
                    }
                }
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            } else {
                global $conn;
                $studentgroup = new Admin($conn);
                //if the new studentgroup_title isn't already in the db, it will be created through this method and vice versa
                if (!$studentgroup->checkIfExistsStudentgroup($this->id_subgroup,$this->id_years)) {
                    $is_created = $studentgroup->newStudentgroup($this->start_year, $this->finish_year, $this->main_teacher_id, $this->id_subgroup, $this->id_years);
                    if($is_created){
                      $msg = 'Uspesno ste kreirali novo odeljenje!';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
                    } else{
                      $msg = 'Pokusajte ponovo, nesto je pogresno!';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                    }
                }else{
                      $msg = 'Odeljenje vec postoji, unesite drugi naziv';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                } 
            }
        }
    }

    /*
	 * Method for displaying schedule page! 
     */
    public function schedule()  
	{ 
        global $conn;                                        
        $admin = new Admin($conn); 
        if(isset($_POST['studentgroup'])) {
            $this->id_sg = $_POST['studentgroup'];   
        } 
        else {
            echo "Izaberite odeljenje";
        }
           
        View::$data['all_studentgroups'] = $admin->getAllStudentgroups();
        if(isset($_POST['studentgroup']) && !empty($_POST['studentgroup'])) {
        $_SESSION['sg_schedule'] = $admin->getScheduleByIdStudentGroup($_POST['studentgroup']);  
        }
        $_SESSION['new_schedule'] = $this->getSortedSchedule();
        View::load('admin', 'schedule');
        
    }
   

    protected function getSortedSchedule()   
    {
        if(isset($_SESSION['sg_schedule']) && $_SESSION['sg_schedule'] !== false){
            $schedule = $_SESSION['sg_schedule'];
            $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
            $sorted = [];

            foreach ($days as $day) {
                for ($i = 1; $i <= 7 ; $i++) {
                    foreach ($schedule as $cas) {
                        if ($cas['day'] == $day && $cas['class'] == $i) {
                            $sorted[$i][$day] = $cas['subject_title'];
            View::$data = $sorted; 
                        }
                    }
                }
            }
        }
        // var_dump(View::$data);
    }

    /*
	 * Method for displaying scheduleform page! (instantiate scheduleform page);
     */
    public function scheduleform()
    {
        global $conn;
        $scheduleform = new Admin($conn);
        View::$data['all_lecturer_subjects'] = $scheduleform->getlecturer_subjects();
        View::$data['all_studentgroups'] = $scheduleform->getAllStudentgroups();
    	View::load('admin', 'scheduleform');
    }

    public function newSchedule()
    {
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->id_student_group = $_POST['studentgroup'];
            $this->day = $_POST['day'];
            $this->class = $_POST['class'];
            $this->semester = $_POST['semester'];
            $this->type = $_POST['type'];
            $this->id_lecturer_subject = $_POST['id_lecturer_subject'];

            if (empty($this->id_student_group)) {
                $this->error['id_student_group'] = 'Izaberite odeljenje!';
            }
            if (empty($this->semester)) {
                $this->error['semester'] = 'Izaberite semestar!';
            }
            if (empty($this->id_lecturer_subject)) {
                $this->error['id_lecturer_subject'] = 'Izaberite id_lecturer_subject!';
            }
            
            
            //if there are some errors add ?, if there's only one err add =error + msg, if there are more errs add & after each err msg.
            if (!empty($this->error)) {
                $errors = '?';
                $lastKey = $this->_array_key_last($this->error);
                foreach ($this->error as $error => $msg) {
                    if ($error == $lastKey) {
                        $errors .= $error . '=' . $msg;
                    } else {
                        $errors .= $error . '=' . $msg . '&';
                    }
                }
                header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
            } else {
                global $conn;
                $makeschedule = new Admin($conn);
                //if the new subgroup_title isn't already in the db, it will be created through this method and vice versa
                //if (!$subgroup->checkIfExistsSubgroup($this->subgroup_title)) {          //staro, ali možda će se nešto slično praviti
                 /*   $is_created = */ $makeschedule->createSchedule($this->id_student_group, $this->day, $this->class, $this->semester, $this->type, $this->id_lecturer_subject);
                 /*   if($is_created){  */
                    if($makeschedule){
                      $msg = 'Uspesno ste kreirali novi raspored!';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?success=' . $msg);
                    } else{
                      $msg = 'Pokusajte ponovo, nesto je pogresno!';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                    }
              /*} else{
                      $msg = 'Raspored vec postoji, unesite drugo odeljenje';
                      header('Location: '. $_SERVER['HTTP_REFERER'] .'?error=' . $msg);
                  }  */
            }
        }
    }

/*
ne pisati:                                       if (isset($_POST)) 

već:                                             if(isset($_POST['neko_polje']))
                
Ili proveravaj da li je poslat post zahtev:      if($_SERVER['REQUEST_METHOD'] === 'POST')
               
Ili proveravaj da li ima necega u $_POST nizu:   if(!empty($_POST))    */
                


}