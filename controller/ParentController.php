<?php

class ParentController
{
    private $err = 'Something went wrong, please try again';

    public function parentpage()
    {
        global $conn;
        $parent = new Parents($conn);
        //get the student for the parent id
        $_SESSION['student'] = $parent->getStudent($_SESSION['user']['id_user']);
        //get the main teacher for the student
        $_SESSION['mainteacher'] = $parent->getMainTeacher($_SESSION['student']['id_student']);
        View::load('parent', 'parentpage');
    }

    public function logout()
    {
        unset($_SESSION['rola']);
        header('Location:/login/login');
    }
}